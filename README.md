# PTCGO To PTCGL Deck Converter

This command line tool takes deck lists exported from Pokemon TCG Online and makes them compatible with Pokemon TCG Live.

For offline use the tool requires the set data to be within the jar itself. You can download it [here](https://github.com/PokemonTCG/pokemon-tcg-data/tree/master/sets). It must be renamed to `sets.json`.

You can use your deck list as a command line argument but you might have issues with new lines. Alternatively you can put your deck list in a text file and use that file's directory as the argument. Here's an example:

`
java -jar Converter.jar /path/to/deck.txt
`

This tool requires the GSON library.
