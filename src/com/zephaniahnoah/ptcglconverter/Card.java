package com.zephaniahnoah.ptcglconverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Card {

	public int count;
	public String name = "";
	public Set set;
	public int number;

	public Card(int count, String name, String set, int number) {
		this.count = count;
		this.name = name;
		this.set = Set.getSet(set);
		this.number = number;
	}

	public Card(String toParse) {
		List<String> words = new ArrayList<String>(Arrays.asList(toParse.split(" ")));

		count = Integer.parseInt(words.get(0));

		words.remove(0);

		number = Integer.parseInt(words.get(words.size() - 1));

		set = Set.getSet(words.get(words.size() - 2));

		words.remove(words.size() - 1);
		words.remove(words.size() - 1);

		for (int i = 0; i < words.size(); i++) {
			name += words.get(i);
			if (i < words.size() - 1) {
				name += " ";
			}
		}
	}

	public void print() {
		if (set == null) {
			System.out.println("Failed to find set for " + name + " " + number);
		}

		System.out.println(toString());
	}

	@Override
	public String toString() {
		return count + " " + name + " " + set.ptcgoCode + " " + number;
	}

	public void validate() {
		if (number > set.total) {
			// Card might be a TG card

			Set tgSet = Set.getTGSet(set);

			if (tgSet == set) {
				return;
			}

			// Reset the number
			number = number - set.total;

			// Set the set type to TG (I.E. If the set is LOR it will now be LOR-TG)
			set = tgSet;
		}
	}
}
