package com.zephaniahnoah.ptcglconverter;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) {
		String deck = "";

		try {

			if (args.length > 0) {
				if (args[0].split("\n").length > 1) {
					deck = args[0];
				} else {
					Path path = Paths.get(args[0]);

					if (Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
						byte[] encoded = Files.readAllBytes(path);

						deck = new String(encoded, StandardCharsets.UTF_8);

					} else {
						System.out.println("File " + args[0] + " was not found.");
						return;
					}
				}
			} else {
				System.out.println("No input data.");
				System.out.println("Try: java -jar Converter.jar /Path/To/Deck.txt");
				return;
			}

			Set.parseSetData();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String newDeck = "";
		boolean firstLine = true;

		for (String line : deck.split("\n")) {
			boolean didThing = false;
			if (line.startsWith("##Trainer Cards")) {
				newDeck += "\nTrainer: " + line.replaceAll("##Trainer Cards - ", "");
				didThing = true;
			} else if (line.startsWith("##")) {
				if (!firstLine) {
					newDeck += "\n";
				}
				newDeck += line.replaceAll("##", "").replaceAll(" - ", ": ");
				firstLine = false;
				didThing = true;
			}

			if (!didThing) {
				char[] chars = line.toCharArray();
				if (chars.length > 0 && chars[0] == (char) 0x2A && chars[1] == (char) 0x20) {
					String word = line.substring(2, line.length());

					// Remove card numbers like 005 or 076 which aren't supported by PTCGL
					word = word.replaceAll(" 00", " ");
					word = word.replaceAll(" 0", " ");

					Card card = new Card(word);

					card.validate();
					// card.print();

					newDeck += card;
					didThing = true;
				} else if (line.startsWith("Total Cards")) {
					newDeck += "\n" + line.replaceAll(" - ", ": ");
					didThing = true;
				}
			}

			if (didThing)
				newDeck += "\n";
		}

		System.out.println(newDeck);
	}
}
