package com.zephaniahnoah.ptcglconverter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

public class Set {

	private static final String url = "https://api.pokemontcg.io/v2/sets";
	private static final Gson gson = new Gson();
	private static final Map<String, Set> sets = new HashMap<String, Set>();
	private static final Map<String, Set> setByCode = new HashMap<String, Set>();
	public String id;
	public String name;
	public String series;
	public int printedTotal;
	public int total;
	public String ptcgoCode;

	public class SetData {
		Set[] data;
	}

	public static void parseSetData() {

		Set[] setArray;

		try {
			setArray = gson.fromJson(new StringReader(getSets()), SetData.class).data;
		} catch (Exception e) {
			System.out.println("Failed to get set data from " + url + ". Attempting to load internal json.");
			setArray = gson.fromJson(new InputStreamReader(Set.class.getResourceAsStream("/sets.json")), Set[].class);
		}

		for (Set set : setArray) {
			// Convert PTCGO code to PTCGL code
			if (set.id.endsWith("tg")) {
				set.ptcgoCode = set.ptcgoCode + "-TG";
			}
			setByCode.put(set.ptcgoCode, set);

			sets.put(set.id, set);
		}
	}

	public static String getSets() throws Exception {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		// int responseCode = con.getResponseCode();
		// System.out.println("\nSending 'GET' request to URL : " + url);
		// System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}

	public static Set getSet(String code) {
		Set set = setByCode.get(code);
		if (set == null) {
			setByCode.put(code, set = new Set());
			set.ptcgoCode = code;
		}

		return setByCode.get(code);
	}

	public static Set getTGSet(Set set) {
		Set tgSet = sets.get(set.id + "tg");
		if (tgSet == null) {
			// System.out.println("Failed to get TG set from " + set.id);
			return set;
		}
		return tgSet;
	}
}
